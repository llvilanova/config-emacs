#!/usr/bin/env python

"""(>>>POINT<<<)"""

__author__     = "(>>>USER_NAME<<<)"
__copyright__  = "Copyright (>>>YEAR<<<), (>>>USER_NAME<<<)"
__license__    = "GPL version 3 or later"

__maintainer__ = "(>>>USER_NAME<<<)"
__email__      = "(>>>AUTHOR<<<)"

(>>>MARK<<<)
>>>TEMPLATE-DEFINITION-SECTION<<<
"...file description; C-x C-x to jump to body..."
("tab" (insert tab-width))
